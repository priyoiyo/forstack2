import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import List from './Pages/List';
import LoginForm from './Component/LoginForm'
import './Component/LoginForm.css'
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    const App = () => (
      <div>
        <BrowserRouter>
     
          <Route exact path='/' component={LoginForm}/>
          <Route exact path='/' component={Home}/>
          <Switch>
          <Route path='/list' component={List}/>
          </Switch>
        </BrowserRouter>
      </div>
    )
    return (
      <BrowserRouter>
  
        <App/>
     
      </BrowserRouter>
    );
  }
}

export default App;
