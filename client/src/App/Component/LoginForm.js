import React from 'react';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import {Link, Redirect} from "react-router-dom";
import axios from 'axios'

class LoginForm extends React.Component{
    constructor(props){
        super(props);
      this.state={
          name:"",
          email:"",
          urlPhoto:"",
          clicked:false
      }
    };




render() {
    
    return(
        <div>
            <div className="guestform flip-top">
          <Container className="gf-container">
            <Row>
              <Col className=" gf-box-col-1">
                <h1 className="gf-text-hello"> Sign Up</h1>
              </Col>
              </Row>
              <Row>
              <Col className="gf-box-col-2 ">
                <p className="gf-text-1">Daftar dulu lur!!</p>
                <Form className="form-kiri">
        <FormGroup>
          <Label>Nama</Label>
          <Input onChange={this.nameChange} value={this.state.name} placeholder="Masukkan Nama" />
        </FormGroup>
        <FormGroup>
          <Label>Email</Label>
          <Input onChange={this.emailChange} value={this.state.email} placeholder="Masukkan Email" />
          
        </FormGroup>
        <FormGroup>
          <Label>Password aja</Label>
          <Input onChange={this.UrlPhotoChange} value={this.state.urlPhoto} placeholder="Masukkan Password" />
          
        </FormGroup>
        <Link to="/Home"><Button id="submit-form" onClick={this.onSubmit}>Sign Up</Button></Link>
        
        </Form>
              </Col>
            </Row>
            
          </Container>
          <Container className="lewati">
          <Link to="/Home"><Button>Login</Button></Link>
          </Container>

          </div>
        </div>
    );

}
nameChange = (event) => {
    this.setState({
        name: event.target.value
        
    })
}
emailChange = (event) => {
    this.setState({
        email: event.target.value
        
    })
}
UrlPhotoChange = (event) => {
  this.setState({
      urlPhoto: event.target.value
      
  })
}
onSubmit = () => {
    console.log(this.state)

    axios.post('https://reduxblog.herokuapp.com/api/posts?key=tugas-pri-1-portofolio', {
      title:this.state.name,
        categories:this.state.email,
        content:this.state.urlPhoto
    })
    
  //   fetch("https://reduxblog.herokuapp.com/api/posts?key=tugas-pri-1-portofolio", {
  //     method: 'POST', // *GET, POST, PUT, DELETE, etc.
  //     mode: 'cors', // no-cors, cors, *same-origin
  //     cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
  //     credentials: 'same-origin', // include, *same-origin, omit
  //     headers: {
  //         'Content-Type': 'application/json',
  //         // 'Content-Type': 'application/x-www-form-urlencoded',
  //     },
  //     redirect: 'follow', // manual, *follow, error
  //     referrer: 'no-referrer', // no-referrer, *client
  //     body: JSON.stringify({
  //       title:this.state.name,
  //       categories:this.state.email,
  //       content:this.state.urlPhoto}

  //     ), // body data type must match "Content-Type" header
  // })
  // .then(response => response.json()); // parses JSON response into native JavaScript objects 
}
    
}


export default LoginForm;